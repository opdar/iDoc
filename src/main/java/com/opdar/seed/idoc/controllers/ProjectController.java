package com.opdar.seed.idoc.controllers;

import com.opdar.framework.db.interfaces.IDao;
import com.opdar.framework.web.SeedWeb;
import com.opdar.framework.web.anotations.Before;
import com.opdar.framework.web.anotations.Controller;
import com.opdar.framework.web.anotations.Inject;
import com.opdar.framework.web.anotations.Router;
import com.opdar.framework.web.interfaces.View;
import com.opdar.framework.web.views.DefaultView;
import com.opdar.framework.web.views.RedirectView;
import com.opdar.seed.extra.utils.DecoderLoader;
import com.opdar.seed.idoc.auth.AuthInterceptor;
import com.opdar.seed.idoc.base.TemplateView;
import com.opdar.seed.idoc.beans.ProjectEntity;
import com.opdar.seed.idoc.beans.ShareVarEntity;
import com.opdar.seed.idoc.services.InterfaceService;
import com.opdar.seed.idoc.services.ProjectService;

import java.util.*;

/**
 * Created by 俊帆 on 2015/8/14.
 */

@Controller(value = "/project", prefix = "html")
@Before(AuthInterceptor.class)
public class ProjectController {

    @Inject
    ProjectService projectService;

    @Inject
    InterfaceService interfaceService;

    @Router("create")
    public View create(String projectName) {
        if (projectName == null) return new RedirectView("../create.html");
        ProjectEntity project = projectService.create(projectName);
        if (project == null) return new RedirectView("../create.html");
        return new RedirectView("../interface/" + project.getId() + ".html");
    }

    @Router("#{projectId}/count")
    public View count(String projectId) {
        int count = interfaceService.findInterfaceCountWithProject(projectId);
        return new DefaultView("<p>项目共计" + count + "个接口</p>");
    }

    @Router("sharevar")
    public View shareVar(String projectId) {
        int count = interfaceService.findInterfaceCountWithProject(projectId);
        return new DefaultView("<p>项目共计" + count + "个接口</p>");
    }

    @Router("params")
    public View params() {
        String projectId = (String) SeedWeb.SharedRequest().get().getSession().getValue("projectId");
        HashMap<String,Object> dataModel = new HashMap<String,Object>();
        dataModel.put("sharevars", projectService.findShareVars(projectId));
        return new TemplateView("projects/params.html", dataModel);
    }

    @Router("addParams")
    public View addParams(String key,String value) {
        String projectId = (String) SeedWeb.SharedRequest().get().getSession().getValue("projectId");
        ShareVarEntity sharevar = new ShareVarEntity();
        sharevar.setProjectId(projectId);
        sharevar.setId(UUID.randomUUID().toString());
        sharevar.setKey(key);
        sharevar.setValue(value);
        return new DefaultView(projectService.insertShareVar(sharevar));
    }

    @Router("removeParams")
    public View removeParams(String id) {
        return new DefaultView(projectService.delShareVar(id));
    }

    @Router("decoders")
    public View decoders() {
        List<String> list = new LinkedList<String>();
        list.add("default");
        for(Iterator<String> it = DecoderLoader.packages.keySet().iterator();it.hasNext();){
            String key = it.next();
            list.add(key);
        }
        Map<String, Object> dataModel = new HashMap<String, Object>();
        dataModel.put("list",list);
        return new TemplateView("decoders/choice.html",dataModel);
    }

    @Router("switch/decoder")
    public View switchDecoder(String decoder) {
        if(DecoderLoader.packages.containsKey(decoder) || decoder.equals("default")){
            return new DefaultView(projectService.switchDecoder(decoder));
        }
        return new DefaultView(false);
    }
}
