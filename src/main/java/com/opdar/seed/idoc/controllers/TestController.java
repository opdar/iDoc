package com.opdar.seed.idoc.controllers;

import com.opdar.framework.web.anotations.Controller;
import com.opdar.framework.web.anotations.RequestBody;
import com.opdar.framework.web.anotations.Router;
import com.opdar.seed.idoc.beans.TestBean;

/**
 * Created by 俊帆 on 2015/8/22.
 */

@Controller(value = "/test", prefix = "run")
public class TestController {

    @Router
    public Object normal(){
        return "hello,test.run page!";
    }

    @Router
    public Object json(){
        TestBean testBean = new TestBean();
        return testBean;
    }

    @Router
    public Object param(String p1,String p2){
        return String.format("p1 = [%s],p2 = [%s]",p1,p2);
    }

    @Router
    public Object postjson(@RequestBody TestBean test){
        return String.format("%s,hashCode[%d]",test.getClass().getSimpleName(),test.hashCode());
    }
}
