package com.opdar.seed.idoc.controllers;

import com.opdar.framework.db.interfaces.IDao;
import com.opdar.framework.db.interfaces.IDatabase;
import com.opdar.framework.web.SeedWeb;
import com.opdar.framework.web.anotations.Before;
import com.opdar.framework.web.anotations.Controller;
import com.opdar.framework.web.anotations.Inject;
import com.opdar.framework.web.anotations.Router;
import com.opdar.framework.web.interfaces.View;
import com.opdar.framework.web.views.DefaultView;
import com.opdar.framework.web.views.RedirectView;
import com.opdar.seed.extra.utils.crypto.MD5;
import com.opdar.seed.idoc.auth.AuthInterceptor;
import com.opdar.seed.idoc.auth.AuthUtils;
import com.opdar.seed.idoc.base.TemplateView;
import com.opdar.seed.idoc.beans.ProjectEntity;
import com.opdar.seed.idoc.beans.UsersEntity;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 俊帆 on 2015/8/21.
 */

@Controller(value = "/", prefix = "html")
@Before(AuthInterceptor.class)
public class IndexController {

    @Inject
    IDatabase database;

    @Inject
    AuthUtils authUtils;

    @Inject
    Logger logger;

    @Router("index")
    public TemplateView index() {
        IDao<ProjectEntity> projectDao = database.getDao(ProjectEntity.class);
        List<ProjectEntity> list = projectDao.select().end().findAll();
        Map<String, Object> dataModels = new HashMap<String, Object>();
        dataModels.put("projects", list);
        return new TemplateView("index.html", dataModels);
    }

    @Router("create")
    public TemplateView create() {
        Map<String, Object> dataModels = new HashMap<String, Object>();
        return new TemplateView("projects/create.html", dataModels);
    }

    @Router("about")
    public TemplateView about() {
        Map<String, Object> dataModels = new HashMap<String, Object>();
        return new TemplateView("about.html", dataModels);
    }

    @Router("login")
    public View login(String userName, String passWord) {
        logger.info("Login pwd [{}]",passWord);
        String md5 = MD5.encrypt(passWord);
        logger.info("Login MD5 pwd [{}]",md5);
        String token = authUtils.login(userName,md5 );
        logger.info("Login Token [{}]",token);
        if (token != null) {
            SeedWeb.SharedRequest().get().getSession().setValue("token", token);
        }
        return new RedirectView(TemplateView.getContextPath().toString());
    }

    @Router("logout")
    public View logout() {
        authUtils.logout();
        return new RedirectView(TemplateView.getContextPath().toString());
    }

    @Router("oneKeyRegist")
    public View oneKeyRegist(String userName) {
        return new DefaultView(authUtils.oneKeyRegist(userName));
    }

    @Router("modifyPwd")
    public View modifyPwd(String userName, String passWord) {
        UsersEntity entity = authUtils.findByUserName(userName);
        String md5 = MD5.encrypt(passWord);
        logger.info("modify pwd md5 [{}]",md5);
        return new DefaultView(authUtils.modifyPwd(entity.getId(),md5));
    }

    @Router("review")
    public View review() {
        return new TemplateView("review.html", null);
    }
}
