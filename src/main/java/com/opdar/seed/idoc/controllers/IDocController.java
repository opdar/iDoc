package com.opdar.seed.idoc.controllers;

import com.opdar.framework.db.interfaces.IDao;
import com.opdar.framework.db.interfaces.IDatabase;
import com.opdar.framework.web.anotations.Controller;
import com.opdar.framework.web.anotations.Inject;
import com.opdar.framework.web.anotations.Router;
import com.opdar.framework.web.interfaces.View;
import com.opdar.seed.idoc.base.TemplateView;
import com.opdar.seed.idoc.beans.IDocEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 俊帆 on 2015/8/19.
 */

@Controller(value = "/idoc", prefix = "html")
public class IDocController {

    @Inject
    IDatabase database;

    @Router("find")
    public View find() {
        IDao<IDocEntity> idocDao = database.getDao(IDocEntity.class);
        Map<String, Object> dataModels = new HashMap<String, Object>();
        dataModels.put("interfaces", idocDao.select().end().findAll());
        return new TemplateView("index.html", dataModels);
    }
}
