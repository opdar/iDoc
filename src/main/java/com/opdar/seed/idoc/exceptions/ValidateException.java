package com.opdar.seed.idoc.exceptions;

/**
 * Created by 俊帆 on 2015/8/21.
 */
public class ValidateException extends Throwable{
    public ValidateException(String field,String reason) {
        super(String.format("Validate [%s] Error,Reason:%s",field,reason));
    }
}
