package com.opdar.seed.idoc.beans;

import com.opdar.seed.extra.utils.StringUtils;

import java.sql.Timestamp;

/**
 * Created by 俊帆 on 2015/8/19.
 */
public class InterfaceEntity {
    private String id;
    private String interfaceName
    ;
    private String projectId;
    private String headers;
    private String postParams;
    private String queryString ;
    private String url;
    private String body;
    private Timestamp createTime;
    private Timestamp updateTime;
    private String result;

    public String getResult() {
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        if(StringUtils.isBlank(interfaceName))interfaceName = "(无名称)";
        this.interfaceName = interfaceName;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        if(StringUtils.isBlank(headers))headers = "";
        this.headers = headers;
    }

    public String getPostParams() {
        return postParams;
    }

    public void setPostParams(String postParams) {
        if(StringUtils.isBlank(postParams))postParams = "";
        this.postParams = postParams;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        if(StringUtils.isBlank(queryString))queryString = "";
        this.queryString = queryString;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        if(StringUtils.isBlank(body))body = "";
        this.body = body;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
