package com.opdar.seed.idoc.beans;

/**
 * Created by 俊帆 on 2015/8/19.
 */
public class IDocOlderVersionEntity {

    private Integer idocId;
    private String key;
    private String value;
    private Integer version;
    private IDocOlderVersionEntity(IDocEntity iDocEntity){
        key = iDocEntity.getKey();
        value = iDocEntity.getValue();
        version = iDocEntity.getVersion();
        idocId = iDocEntity.getId();
    }

    public Integer getIdocId() {
        return idocId;
    }

    public void setIdocId(Integer idocId) {
        this.idocId = idocId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
