package com.opdar.seed.idoc.beans;

import java.io.Serializable;

/**
 * Created by 俊帆 on 2015/8/19.
 */
public class HeaderEntity implements Serializable {
    private String headerKey;
    private String headerValue;

    public HeaderEntity() {
    }

    public HeaderEntity(String headerKey, String headerValue) {

        this.headerKey = headerKey;
        this.headerValue = headerValue;
    }

    public String getHeaderKey() {
        return headerKey;
    }

    public void setHeaderKey(String headerKey) {
        this.headerKey = headerKey;
    }

    public String getHeaderValue() {
        return headerValue;
    }

    public void setHeaderValue(String headerValue) {
        this.headerValue = headerValue;
    }
}
