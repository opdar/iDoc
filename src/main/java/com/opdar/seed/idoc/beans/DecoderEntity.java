package com.opdar.seed.idoc.beans;

/**
 * Created by 俊帆 on 2015/8/23.
 */
public class DecoderEntity {

    String id;
    String decoderName;
    String decoderPath;
    String createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDecoderName() {
        return decoderName;
    }

    public void setDecoderName(String decoderName) {
        this.decoderName = decoderName;
    }

    public String getDecoderPath() {
        return decoderPath;
    }

    public void setDecoderPath(String decoderPath) {
        this.decoderPath = decoderPath;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
