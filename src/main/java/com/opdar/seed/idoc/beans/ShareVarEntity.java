package com.opdar.seed.idoc.beans;

import com.opdar.framework.db.anotations.Table;

/**
 * Created by 俊帆 on 2015/8/22.
 */
@Table("t_share_var")
public class ShareVarEntity {
    private String id;
    private String projectId;
    private String key;
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
