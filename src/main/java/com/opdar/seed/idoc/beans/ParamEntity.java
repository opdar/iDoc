package com.opdar.seed.idoc.beans;

import java.io.Serializable;

/**
 * Created by 俊帆 on 2015/8/19.
 */
public class ParamEntity implements Serializable {
    private String paramKey;
    private String paramValue;

    public ParamEntity() {
    }

    public String getParamKey() {
        return paramKey;
    }

    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}
