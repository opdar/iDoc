package com.opdar.seed.idoc.beans;

/**
 * Created by 俊帆 on 2015/8/22.
 */
public class ResultEntity {
    private int code = 0;
    private String message;
    private Object data;

    public static ResultEntity getFailure(int code,String message,Object data){
        return new ResultEntity(0,message,data);
    }

    public static ResultEntity getSuccess(String message,Object data){
        return new ResultEntity(0,message,data);
    }

    public static ResultEntity getSuccess(Object data){
        return new ResultEntity(0,"操作成功",data);
    }

    private ResultEntity(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
