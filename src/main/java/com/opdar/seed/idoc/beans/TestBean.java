package com.opdar.seed.idoc.beans;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by 俊帆 on 2015/8/14.
 */
public class TestBean {
    private String field1;
    private Integer field2;
    private Date field3;
    private Timestamp field4;

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public Integer getField2() {
        return field2;
    }

    public void setField2(Integer field2) {
        this.field2 = field2;
    }

    public Date getField3() {
        return field3;
    }

    public void setField3(Date field3) {
        this.field3 = field3;
    }

    public Timestamp getField4() {
        return field4;
    }

    public void setField4(Timestamp field4) {
        this.field4 = field4;
    }
}
