package com.opdar.seed.idoc.beans;

import com.opdar.framework.db.anotations.Table;

/**
 * Created by 俊帆 on 2015/8/19.
 */
@Table("t_idoc")
public class IDocEntity {
    private Integer id;
    private String key;
    private String value;
    private Integer version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
