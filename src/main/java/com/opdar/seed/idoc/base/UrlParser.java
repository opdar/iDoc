package com.opdar.seed.idoc.base;

import com.opdar.framework.template.base.Printf;
import com.opdar.framework.template.parser.Parser;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 俊帆 on 2015/8/22.
 */
public class UrlParser {

    private static final Map<String,Object> globalParams = new HashMap<String, Object>();

    public static String get(String url,HashMap<String,Object> params){
        StringWriter sw = new StringWriter();
        try{
            Parser parser = new Parser("${", "}");
            parser.get(url);
            if(params != null){
                parser.parse(globalParams, sw ,params);
            }else{
                parser.parse(globalParams, sw);
            }
            return sw.toString();
        }finally {
            sw.flush();
            try {
                sw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
