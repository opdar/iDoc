package com.opdar.seed.idoc.base;

import com.opdar.framework.db.impl.BaseWhere;
import com.opdar.framework.server.base.DefaultConfig;
import com.opdar.framework.server.supports.jetty.JettySupport;
import com.opdar.framework.web.common.Context;
import com.opdar.seed.extra.utils.DecoderLoader;
import com.opdar.seed.idoc.controllers.ProjectController;
import com.opdar.seed.idoc.services.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Properties;

/**
 * Created by Jeffrey on 2015/4/22.
 * E-Mail:shijunfan@163.com
 * Site:opdar.com
 * QQ:362116120
 */
public class TomcatConfig extends DefaultConfig {

    Properties properties = new Properties();

    public TomcatConfig() {
        load("/configures/config.properties");
    }

    public void load(String propertiesFile) {
        try {
            properties.load(TomcatConfig.class.getResourceAsStream(propertiesFile));

            setProperties(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new JettySupport(8080).config(new TomcatConfig()).start();
    }

    @Override
    public void onCreate() {
        BaseWhere.Order createTimeDesc = new BaseWhere.Order("create_time", BaseWhere.Order.OrderType.DESC);
        Context.add("createTimeDesc", createTimeDesc);
        DecoderLoader.loadJarPath(new File(System.getProperty("seed.root"), "Decoders").getAbsolutePath());
        Logger logger = LoggerFactory.getLogger("iDoc");
        Context.add(Logger.class.getName(),logger);
        logger.info("iDoc onCreate");
    }

    @Override
    public void onDestory() {
        System.out.println("onDestory");
    }
}
