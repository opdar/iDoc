package com.opdar.seed.idoc.base;

import com.opdar.framework.template.parser.ClassPathResourceLoader;
import com.opdar.framework.web.SeedWeb;
import com.opdar.framework.web.common.HttpResponseCode;
import com.opdar.framework.web.interfaces.View;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by 俊帆 on 2015/8/21.
 */
public class TemplateView implements View {
    String contentType = "text/html";

    String templatePath = null;
    Object dataModels = null;
    private static ClassLoader classLoader = TemplateView.class.getClassLoader();

    private static final ClassPathResourceLoader loader = new ClassPathResourceLoader(classLoader);

    static{
        loader.setBasePath("templates/");
    }

    public static void setContextPath(String contextPath){
        loader.setGlobalVars("contextPath",contextPath);
    }

    public static void setGlobalVar(String key,String value){
        loader.setGlobalVars(key,value);
    }
    public static Object getContextPath(){
        return loader.getGlobalVars().get("contextPath");
    }

    public TemplateView(String templatePath, Object dataModels) {
        this.templatePath = templatePath;
        this.dataModels = dataModels;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public Map<String, String> headers() {
        return null;
    }

    public byte[] renderView() {
        try {
            return loader.parse(templatePath, dataModels).getBytes("utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    public String contentType() {
        return contentType;
    }

    public int getCode() {
        return HttpResponseCode.CODE_200.getCode();
    }
}