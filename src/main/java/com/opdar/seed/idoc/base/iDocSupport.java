package com.opdar.seed.idoc.base;

import com.opdar.framework.server.supports.servlet.ServletSupport;

import javax.servlet.ServletContextEvent;

/**
 * Created by 俊帆 on 2015/8/24.
 */
public class iDocSupport extends ServletSupport {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        String contentPath = servletContextEvent.getServletContext().getContextPath();
        TemplateView.setContextPath(contentPath+"/");
        TemplateView.setGlobalVar("isLogin","false");
        super.contextInitialized(servletContextEvent);
    }
}
