package com.opdar.seed.idoc.base;

import com.opdar.framework.web.interfaces.View;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;

/**
 * Created by 俊帆 on 2015/8/23.
 */
public class MDView implements View {
    private byte[] data;
    private String fileName = "unname.zip";
    public MDView(byte[] data, String fileName) {
        this.data = data;
        this.fileName = fileName;
    }

    @Override
    public Map<String, String> headers() {
        Map<String,String> headers = new HashMap<String, String>();
        headers.put("Content-Disposition","attachment; filename="+fileName);
        return headers;
    }

    @Override
    public byte[] renderView() {
        byte[] b = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            GZIPOutputStream gzip = new GZIPOutputStream(bos);
            gzip.write(data);
            gzip.finish();
            gzip.close();
            b = bos.toByteArray();
            bos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return b;
    }

    public String contentType() {
        return "application/octet-stream";
    }

    public int getCode() {
        return 200;
    }
}
