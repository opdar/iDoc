package com.opdar.seed.idoc.auth;

import com.opdar.framework.web.SeedWeb;
import com.opdar.seed.extra.utils.StringUtils;
import com.opdar.seed.idoc.base.TemplateView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by 俊帆 on 2015/8/25.
 */
public class AuthInterceptor {

    private Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);

    public Object before(){
        Object token = SeedWeb.SharedRequest().get().getSession().getValue("token");
        logger.info("Auth Token [{}]",token);
        if(!StringUtils.isBlank(token)){
            TemplateView.setGlobalVar("isLogin","true");
        }else{
            TemplateView.setGlobalVar("isLogin","false");
        }
        return true;
    }
}
