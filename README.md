#iDoc

#### 使用前先Clone [Seed](http://git.oschina.net/opdar/Seed) 并运行 mvn install安装至本地。或者可去附件下载编译后的war包，直接放至容器下运行。

### 关于编码器

在请求完成后，可以针对请求后的数据进行一些解码操作。自定义解码器需要导入seed-extra项目，实现com.opdar.seed.extra.utils.Decoder类。

实现完相应解码后，将编译后的代码打成jar包，并在根目录下添加package.json文件

```
{
  "main":"com.opdar.xxx.DesDecoder",
  "name":"unique-decoder"
}
```

main为Decoder的实现类，name为该解码器的唯一标识

#### QQ群：372824396